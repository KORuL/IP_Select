#ifndef IPSELECTSQLLITE_H
#define IPSELECTSQLLITE_H

#include <QWidget>
#include <QtSql>
#include <QFileInfo>
#include <QSqlDatabase>
#include <QDir>
#include <QMessageBox>
#include <QFileDialog>
#include <QtConcurrent/QtConcurrentRun>

namespace Ui {
class IPSelectSQLlite;
}

class IPSelectSQLlite : public QWidget
{
    Q_OBJECT
    QString LastStrBase;

public:
    explicit IPSelectSQLlite(QWidget *parent = 0);
    ~IPSelectSQLlite();

private slots:
    void on_pushButton_clicked();
    void SaveParametr();
    void LoadParametr();
    bool connectToBase();
    void ZaprosToBase();

    void on_PB_ChooseBaseName_clicked();

private:
    Ui::IPSelectSQLlite *ui;

    QSqlDatabase myDB;
    QSqlQueryModel *model;
};

#endif // IPSELECTSQLLITE_H
