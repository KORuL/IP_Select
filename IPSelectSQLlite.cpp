#include "IPSelectSQLlite.h"
#include "ui_IPSelectSQLlite.h"

IPSelectSQLlite::IPSelectSQLlite(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::IPSelectSQLlite)
{
    ui->setupUi(this);
    LoadParametr();
    model = new QSqlQueryModel(ui->tableView);

    LastStrBase = "";
}

IPSelectSQLlite::~IPSelectSQLlite()
{
    SaveParametr();
    delete ui;
}

void IPSelectSQLlite::SaveParametr()
{
    QSettings settings("KORuL", "SQLLITE");

    QString LE_SQLName   = ui->LE_SQLName  ->text();
    QString LE_TableName = ui->LE_TableName->text();
    QString LE_IPAddress = ui->LE_IPAddress->text();
    QString LE_IPLow     = ui->LE_IPLow    ->text();
    QString LE_IPHigh    = ui->LE_IPHigh   ->text();

    settings.setValue( "LE_SQLName ",  LE_SQLName   );
    settings.setValue( "LE_TableName", LE_TableName );
    settings.setValue( "LE_IPAddress", LE_IPAddress );
    settings.setValue( "LE_IPLow",     LE_IPLow     );
    settings.setValue( "LE_IPHigh",    LE_IPHigh    );
    settings.sync();
}

void IPSelectSQLlite::LoadParametr()
{
    QSettings settings("KORuL", "SQLLITE");

    QString LE_SQLName   = settings.value("LE_SQLName ",  ""      ).toString();
    QString LE_TableName = settings.value("LE_TableName", ""      ).toString();
    QString LE_IPAddress = settings.value("LE_IPAddress", ""      ).toString();
    QString LE_IPLow     = settings.value("LE_IPLow",     "IpLow" ).toString();
    QString LE_IPHigh    = settings.value("LE_IPHigh",    "IpHigh").toString();

    ui->LE_SQLName  ->setText(LE_SQLName  );
    ui->LE_TableName->setText(LE_TableName);
    ui->LE_IPAddress->setText(LE_IPAddress);
    ui->LE_IPLow    ->setText(LE_IPLow    );
    ui->LE_IPHigh   ->setText(LE_IPHigh   );
}

bool IPSelectSQLlite::connectToBase()
{
    myDB = QSqlDatabase::addDatabase("QSQLITE");

    QString pathToDB = ui->LE_SQLName->text();
    myDB.setDatabaseName(pathToDB);

     QFileInfo checkFile(pathToDB);

     if (checkFile.isFile()) {
         if (myDB.open()) {
             LastStrBase = pathToDB;
             return 1;
         }
         else {
             QMessageBox::critical(this, QString("Внимание"), QString("[!] Database File was not opened"), QMessageBox::Yes);
             return 0;
         }
     }
     else {
         QMessageBox::critical(this, QString("Внимание"), QString("[!] Database File does not exist"), QMessageBox::Yes);
         return 0;
     }
}

void IPSelectSQLlite::ZaprosToBase()
{
    QString IP = ui->LE_IPAddress->text();
    QStringList oktet = IP.split(".");

    qlonglong ip = oktet.at(0).toLongLong()*qPow(256,3) + oktet.at(1).toLongLong()*qPow(256,2)
            + oktet.at(2).toLongLong()*qPow(256,1) + oktet.at(3).toLongLong()*qPow(256,0);

    QString zapros = "SELECT * FROM %1 WHERE %2<=%3 AND %4>=%5";
    zapros = zapros.arg(ui->LE_TableName->text()).
            arg(ui->LE_IPLow->text()).arg(ip).
            arg(ui->LE_IPHigh->text()).arg(ip);

    model->setQuery(QSqlQuery(zapros, myDB));
    ui->tableView->setModel(model);
    ui->tableView->show();

    if(model->lastError().isValid())
        QMessageBox::critical(this, QString("Внимание"), QString("[!] " + model->lastError().text()), QMessageBox::Yes);
}


void IPSelectSQLlite::on_PB_ChooseBaseName_clicked()
{
    QString str = QFileDialog::getOpenFileName(0, QString("Load"), QString(""), QString(""));
    ui->LE_SQLName->setText(str);

    if (myDB.open())
        myDB.close();

    connectToBase();
    SaveParametr();
}


void IPSelectSQLlite::on_pushButton_clicked()
{
    bool connecttoBase;
    SaveParametr();

    if(!myDB.isOpen() || LastStrBase != ui->LE_SQLName->text())
        connecttoBase = connectToBase();
    else
        connecttoBase = true;

    if(connecttoBase)
    {
        if (!myDB.isOpen())
        {
            QMessageBox::critical(this, QString("Внимание"), QString("[-] No connection to Database"), QMessageBox::Yes);
            return;
        }

        QtConcurrent::run(this, &IPSelectSQLlite::ZaprosToBase);
    }
}


