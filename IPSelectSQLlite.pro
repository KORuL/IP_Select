#-------------------------------------------------
#
# Project created by QtCreator 2016-11-12T13:21:00
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = IPSelectSQLite
TEMPLATE = app


SOURCES += main.cpp\
        IPSelectSQLlite.cpp

HEADERS  += IPSelectSQLlite.h

FORMS    += IPSelectSQLlite.ui

RC_FILE = myap1.rc

RESOURCES += \
    res.qrc
